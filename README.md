# README #

Maven projects to do Continious Integration and Continious Deployment with Oracle SOA Suite.

### What is this repository for? ###

* Architect
* Developers

### How do I get set up? ###

1. Use branch:

* 12_1_3 for 12.1.3 version
* 12_2_1 for 12.2.1 version

2. Follow README of branch

### Contribution guidelines ###

If you want evolve this scripts, use Pull request.

### Who do I talk to? ###

* Repo owner or admin

Emmanuel LESNE: emmanuel.lesne@middleware-solutions.fr

* Other community or team contact