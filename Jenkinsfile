node('maven') {

   // define project name
   def projectName = "cicd-ofm-parent"

   // define targets
   def osDevTarget = "dev"
   def osStageTarget = "stage"

   def repo-releases = "http://nexus3-cicd.minishift.local/repository/maven-releases/"

   stage ('Install Oracle dependencies') {
    // push maven jar into nexus
    bat "cd %FMW_HOME%\oracle_common\plugins\maven\com\oracle\maven\oracle-maven-sync\12.1.3"
    bat "mvn install:install-file -DpomFile=oracle-maven-sync-12.1.3.pom -Dfile=oracle-maven-sync-12.1.3.jar -Durl=${repo-releases} -DrepositoryId=nexus"

    // just a verification
    sh "mvn help:describe -Dplugin=com.oracle.maven:oracle-maven-sync -Ddetail"

    // push oracle jar and pom
    sh "mvn com.oracle.maven:oracle-maven-sync:push -DoracleHome=%FMW_HOME% -DserverId=nexus"
   }

   stage ('Build') {
	 checkout scm

	 // use a global settings file
	 configFileProvider(
        [configFile(fileId: 'settings-global', variable: 'MAVEN_SETTINGS')]) {
        sh "mvn -s $MAVEN_SETTINGS install -DskipTests=true"
    }
   }

   stage ('Deploy DEV') {
     // clean old build OpenShift
     sh "rm -rf oc-build && mkdir -p oc-build/deployments"

     // copy ressources
     sh "cp target/openshift-tasks.war oc-build/deployments/ROOT.war"
     sh "cp -r ./configuration oc-build/"

     // change project to DEV
     sh "oc project ${osDevTarget}"

     // clean up. keep the image stream
     sh "oc delete bc,dc,svc,route -l app=${projectName} -n ${osDevTarget}"

     // create build. override the exit code since it complains about exising imagestream
     sh "oc new-build --name=${projectName} --image-stream=jboss-eap70-openshift --binary=true --labels=app=${projectName} -n ${osDevTarget} || true"

     // build image
     sh "oc start-build ${projectName} --from-dir=oc-build --wait=true -n ${osDevTarget}"

     // deploy image
     sh "oc new-app ${projectName}:latest -n ${osDevTarget}"
     sh "oc expose svc/${projectName} -n ${osDevTarget}"
   }

   stage ('Unit Tests') {
     // use a global settings file
	 configFileProvider(
        [configFile(fileId: 'settings-global', variable: 'MAVEN_SETTINGS')]) {
		sh "mvn -s $MAVEN_SETTINGS test"
		step([$class: 'JUnitResultArchiver', testResults: '**/target/surefire-reports/TEST-*.xml'])
    }

	 // use a global settings file
	 configFileProvider(
        [configFile(fileId: 'settings-global', variable: 'MAVEN_SETTINGS')]) {
        sh "mvn -s $MAVEN_SETTINGS sonar:sonar -Dsonar.host.url=http://sonarqube:9000 -Dmaven.test.failure.ignore=true"
    }

   }

   stage ('Push to Nexus') {
    // use a global settings file
	configFileProvider(
        [configFile(fileId: 'settings-global', variable: 'MAVEN_SETTINGS')]) {
        sh "mvn -s $MAVEN_SETTINGS deploy -DskipTests=true"
    }

   }

   stage ('Deploy STAGE') {
     timeout(time:5, unit:'MINUTES') {
        input message: "Promote to STAGE?", ok: "Promote"
     }

     def v = version()
     // tag for stage
     sh "oc tag ${osDevTarget}/${projectName}:latest ${osStageTarget}/${projectName}:${v}"

     // change project
     sh "oc project ${osStageTarget}"

     // clean up. keep the imagestream
     sh "oc delete bc,dc,svc,route -l app=${projectName} -n ${osStageTarget}"

     // deploy stage image
     sh "oc new-app ${projectName}:${v} -n ${osStageTarget}"
     sh "oc expose svc/${projectName} -n ${osStageTarget}"
   }

   stage ('Integration Tests') {
   // use a global settings file
	configFileProvider(
        [configFile(fileId: 'settings-global', variable: 'MAVEN_SETTINGS')]) {
        sh "mvn -s $MAVEN_SETTINGS verify"
    }

     step([$class: 'JUnitResultArchiver', testResults: '**/target/failsafe-reports/TEST-*.xml'])

     // use a global settings file
	 configFileProvider(
        [configFile(fileId: 'settings-global', variable: 'MAVEN_SETTINGS')]) {
        sh "mvn -s $MAVEN_SETTINGS sonar:sonar -Dsonar.host.url=http://sonarqube:9000 -Dmaven.test.failure.ignore=true"
    }
   }
}

def version() {
  def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
  matcher ? matcher[0][1] : null
}
